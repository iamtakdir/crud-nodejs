import express from 'express'
import {
    getAllBook,
    getBook,
    createBook,
    updateBook,
    deleteBook
} from '../views/book.js';

const router = express.Router();

// all books 
router.get('/', getAllBook)
//single book
router.get('/:id', getBook)
// Create book 
router.post('/', createBook)
//Update book
router.patch('/:id', updateBook)
//Delete book
router.delete('/:id', deleteBook)

export default router;