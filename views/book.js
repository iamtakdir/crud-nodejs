import {
    PrismaClient
} from "@prisma/client";

const prisma = new PrismaClient();

//get all
export const getAllBook = async (req, res, next) => {

    try {
        const books = await prisma.book.findMany({})

        res.json(books)

    } catch (error) {
        next(error)
    }

}

//get single by id 
export const getBook = async (req, res, next) => {
    try {
        const id = req.params.id
        const book = await prisma.book.findUnique({
            where: {
                id: Number(id)
            }
        })
        res.json(book)
    } catch (error) {
        next(error)
    }


}
//create new
export const createBook = async (req, res, next) => {

    try {
        let data = req.body
        const book = await prisma.book.create({
            data
        })
        res.json({
            "message": "Created",
            "Book ID": book.id
        })
    } catch (error) {
        next(error)
    }
}

//update with id 
export const updateBook = async (req, res, next) => {
    try {
        const id = req.params.id
        let data = req.body

        console.log(id)

        const book = await prisma.book.update({
            where: {
                id: Number(id)
            },
            data: data
        })

        res.json(book)

    } catch (error) {
        next(error)
    }
}
//delete with id 
export const deleteBook = async (req, res, next) => {
    try {
        const id = req.params.id

        const deleteBook = prisma.book.delete({
            where: {
                id: Number(id)
            }
        })
        res.json({
            "message": `Deleted Book ${(await deleteBook).id}`
        })
    } catch (error) {
        next(error)
    }
}